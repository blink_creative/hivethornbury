

$(document).ready(function() {

    // Sticky nav

    $(window).bind('scroll', function(){
        var navHeight = $(window).height() - 74;
            if ($(window).scrollTop() > navHeight){
                $('header').addClass('scrolled');
            }
            else{
                $('header').removeClass('scrolled');
        }
    });

    // Scroll To

    $('.scrollto a').click(function(){
        $.scrollTo( this.hash, 1500, { easing:'swing' });
        return false;
    });

    // Google Maps
    var grayStyles = [
        {
            featureType: "all",
            stylers: [
                { saturation: -90 },
                { lightness: 50 }
            ]
        },
    ];

    var latlng = new google.maps.LatLng(-37.7601275,145.0003395);
    var options = {
        center: latlng,
        zoom: 15,
        disableDefaultUI: true,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: false,
        styles: grayStyles,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'), options);
    var image = new google.maps.MarkerImage('img/map-marker.png',
        new google.maps.Size(50, 43),
        new google.maps.Point(0,0),
        new google.maps.Point(3, 61)
    );

    var marker1 = new google.maps.Marker({
        position: new google.maps.LatLng(-37.7601275,145.0003395),
        map: map,
        animation: google.maps.Animation.DROP,
        icon: image
    });



    // Vertical Carousel
    $('.verticalSlide-container').slick({
        pauseOnHover: false,
        autoplay: true,
        vertical: true,
        verticalSwiping: true,
        arrows: false,
        autoplaySpeed: 4000
    });

    // Second Vertical Carousel
    $('.verticalSlide-container2').slick({
        pauseOnHover: false,
        autoplay: true,
        vertical: true,
        verticalSwiping: true,
        arrows: false,
        autoplaySpeed: 4000
    });

    // Horinzontal Carousel
    $('.horizontalSlide-container').slick({
        respondTo: 'slider',
        pauseOnHover: false,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 4000
    });


    // Team and locals click states
    $('.showBio').on('click', function () {
        $('.showBio.selected').not(this).removeClass("selected");
        $(this).addClass('selected');
        $('body').addClass('noscroll');
        $('.bio-overlay').addClass('visable');
        $('.targetBio').hide();
        $('#bio' + $(this).data('target')).show();
    });

    $('.overlay-button').on('click', function(){
        $('.bio-overlay').removeClass('visable');
        $('body').removeClass('noscroll');
        $('.showBio.selected').not(this).removeClass("selected");

    });

    $('.bio-overlay').on('click', function(){
        $('.bio-overlay').removeClass('visable');
        $('body').removeClass('noscroll');
        $('.showBio.selected').not(this).removeClass("selected");

    })

    //SVG fallback

    if (!Modernizr.svg) {
        $("img[src$='.svg']")
            .attr("src", fallback);
    }

});